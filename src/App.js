import "./App.css";
import About from "./components/About";
import Navbar from "./components/Navbar";

function App() {
  return (
    <>
      <About />
      <Navbar title="TextUtils" aboutText="About Us" />
    </>
  );
}

export default App;
